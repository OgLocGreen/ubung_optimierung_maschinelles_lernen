import numpy

"""
Aufgabe1:
a)
numpy.linalg.qr()
-> qrzerlegung

numpy.matmul()
-> Probe

b)


c)
 numpy.linalg.eigval
"""

A = [[1, 2, 4], [2, 3, 8], [-1, -3, -1]]

q, r = numpy.linalg.qr(A)
A_new = numpy.matmul(q,r)

print(A_new)

A_n = B = [[2,-1],[-1,4]]
i = 0

while(True):
    Q, R = numpy.linalg.qr(A_n)
    A_n = numpy.matmul(R, Q)
    i += 1
    if A_n[0][1] < 0.000001 and A_n[0][1] > -0.000001:
        break
print(i)
print(A_n)

print(numpy.linalg.eigvals(B))

"""
Aufgabe 2:
"""

def solveUpper(R, b_v):
    R_inv = numpy.linalg.inv(R)
    x_v = numpy.matmul(R_inv, b_v)
    return x

def myORSolver(A, b_v):
    Q, R = numpy.linalg.qr(A)
    R_inv = numpy.linalg.inv(R)
    Q_t = numpy.transpose(Q)
    R_Q = numpy.matmul(R_inv, Q)
    x_v = numpy.matmul(R_Q, b_v)
    return x_v

A = [[1,2],[3,1]]
b_v = [1,2]

print(myORSolver(A,b_v))
print(numpy.linalg.solve(A,b_v))


""""
Aufgabe3:
"""

def sampleNoisyPoints(n,noisy):
    sample_list = []
    m = 0.5
    c = 2
    for t in range(n):
        if noisy:
            rauschen = numpy.random.uniform(0, 1)
        else:
            rauschen = 0
        sample_list.append(m*t+c+rauschen)
    return sample_list

print(sampleNoisyPoints(10))

def setupLGS(lambda_s, y_v):
    E_f = []
    for y in y_v:
        E_f.append(1)
    return