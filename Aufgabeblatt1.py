
import matplotlib.pyplot as plt

##Aufgabe 2a)
def func(x, y):
    z = 100 * (y - x**2)**2 + (1 - x)**2
    return z
print(func(1,1))

## Aufgabe 2b)

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator
import numpy as np


X = np.linspace(-5, 5, 50)
Y = np.linspace(-5, 5, 50)
X, Y = np.meshgrid(X, Y)
Z = func(X, Y)

ax = plt.axes(projection='3d')
ax.plot_surface(X, Y,Z, cmap="plasma")
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
plt.show()

## Aufgabe 2c)
from sympy import symbols, diff

def ableitung_fx_2(f, x0, y0):
    x, y = symbols('x y', real=True)
    fx = diff(f, x)
    print(fx)
    x,y = x0, y0
    return fx(x,y)

def ableitung_fy_2(f, x0, y0):
    x, y = symbols('x y', real=True)
    fy = diff(f, y)
    x,y = x0, y0
    return fy(x,y)

x, y = symbols('x y', real=True)
f = 100 * (y - x**2)**2 + (1 - x)**2



x, y = symbols('x y', real=True)
fx = diff(f, x)
fy = diff(f, y)
print(fx)
print(fy)

## Aufgabe 2c)


def ableitung_fx(x,y):
    return -400*x*(-x**2 + y) + 2*x - 2

def ableitung_fy(x,y):
    return -200*x**2 + 200*y

print(ableitung_fx(1, 1))
print(ableitung_fy(1, 1))
print(ableitung_fx(2, 2))
print(ableitung_fy(2, 2))


## Aufgabe 2d)
from numpy.linalg import norm

def richtungsableitung(p,x0):
    return np.dot((ableitung_fx(x0[0], x0[1]), ableitung_fy(x0[0], x0[1])), (p/norm(p)))

x0 = [2,1]
p = [1,1]

print(richtungsableitung(p, x0))


## Aufgabe 2e)
x0 = np.array([2, 1])
p = np.array([1, 1])
aprox = []
epsilon = [1e-08,1e-07,1e-06,1e-05,1e-04,1e-03,1e-02,1e-01]

for e in epsilon:
    new_vec = x0 + e*p
    one = func(new_vec[0], new_vec[1])
    two = func(x0[0], x0[1])
    zwischen = one - two
    ergenis = zwischen / e

    ergebnis = (func(new_vec[0],new_vec[1]) - func(x0[0], x0[1])) / e
    aprox.append(ergebnis)
print(aprox)