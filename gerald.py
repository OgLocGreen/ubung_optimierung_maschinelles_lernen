space1  = [0,1,2,3,4,5,6]
space2 = [0,1,2,3,4,5,6]

def compile_double_solution_space(space1, space2):
    # YOUR CODE HERE
    result = []
    subresult = []
    for x in space1:
        for y in space2:
            result.append([x, y])
    return result

print(compile_double_solution_space(space1,space2))

